using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas : MonoBehaviour
{
    public LayerMask layerMask;
    public float pencilDistance;
    public float pencilSize;
    public Color pencilColor;
    public Transform pencil;
    public float canvasSize;

    Vector3 lastPoint;
    public Vector3 pencilPoint;

    public int drawPerFrame;
    Queue<Vector3Int> toDraw;

    public Texture2D texture;

    void Start()
    {
        toDraw = new Queue<Vector3Int>();
    }

    void Update()
    {
        pencilPoint = RayTrace(pencil);
        if(pencilPoint != default && pencilPoint != lastPoint) 
        { 
            Draw(pencilPoint);
            lastPoint = pencilPoint;
        }

        if(toDraw.Count > 0)
        {
            for(int i = 0; i < drawPerFrame && toDraw.Count > 0;i++)
            {
                Vector3Int p = toDraw.Dequeue();
                Draw(p.x, p.y, pencilColor);
            }
        }
    }

    void Draw(Vector3 point)
    {
        point -= transform.position;
        point /= canvasSize;
        point *= texture.width;

        Debug.Log("Drawing at " + point);

        for(int x = 0; x < pencilSize;x++)
        {
            for(int y= 0; y < pencilSize;y++)
            {
                toDraw.Enqueue(new Vector3Int((int)point.x + x, (int)point.y + y));
                //Draw((int)point.x + x, (int)point.y + y, pencilColor);
            }
        }
    }

    void Draw(int x, int y, Color color)
    {
        if (x >= 0 && x < texture.width && y >= 0 && y < texture.height)
        {
            texture.SetPixel(x, y, color);
            texture.Apply();
        }
    }

    Vector3 RayTrace(Transform t)
    {
        RaycastHit hit;
        Debug.DrawRay(t.position, t.forward * pencilDistance * 10, Color.red);
        if (Physics.Raycast(t.position, t.forward, out hit, pencilDistance, layerMask))
        {
            return hit.point;
        }
        else
        {
            return default;
        }
    }
}
