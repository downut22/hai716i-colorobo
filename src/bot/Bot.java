package bot;

import utilities.HSLColor;
import utilities.Vect3;
import java.awt.*;
import java.io.Console;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import game.Main;
import java.awt.image.BufferedImage;

public class Bot {
	private Vect3 position = new Vect3();
	private double orientation = 0;
	private Vect3 speed = new Vect3();
	private HSLColor color = new HSLColor(new Color(255,0,0)); public HSLColor getColor() {return color;}
	private float size;
	
	private Actions actions;
	Color sight;
	
	void debug()
	{
		System.out.println((int)(orientation * 100000));
		System.out.println(speed.x + "  |  " + speed.y);
	}
	
	void draw()
	{
		Main.canvas.put(
				position.x, 
				position.y,
				color.rgb,(int)Math.round(size),speed);//
	}
	
	void move()//double deltaTime)
	{
		speed = new Vect3(Math.cos(orientation),Math.sin(orientation),0);
		speed.mul(baseSpeed);
		
		position.add(speed);//Vect3.Mul(speed,deltaTime));
		
		if(position.x > 1)
		{
			position.x = 0.01;
			//speed.x = -speed.x;
			//orientation = Math.acos(speed.x / baseSpeed);
		}
		else if(position.x < 0)
		{
			position.x = 0.99;
			//speed.x = -speed.x;
			//orientation = Math.acos(speed.x / baseSpeed);
		}
		if(position.y > 1)
		{
			position.y = 0.01;
			//speed.y = -speed.y;
			//orientation = Math.asin(speed.y / baseSpeed);
		}
		else if(position.y < 0 )
		{
			position.y = 0.99;
			//speed.y = -speed.y;
			//orientation = Math.asin(speed.y / baseSpeed);
		}
	}
	
	public void rotate(double r) 
	{
		orientation += r * baseRotation;
	}
	public void changeColor(double r,int colorElement) 
	{
		if(colorElement == 0) {
			color.hsl[0] += r * hueStep;
			if(color.hsl[0] >= 360) {color.hsl[0] = 0;}
		}
		else {
			color.hsl[1] += r * saturationStep * saturationDir;
			if(color.hsl[1] >= 100) {color.hsl[1] = 99; saturationDir = -saturationDir;}
			else if(color.hsl[1] <= 0) {color.hsl[1] = 1; saturationDir = -saturationDir;}
		}
		
		color.rgb=HSLColor.toRGB(color.hsl);
	}
	public void changeSize(double r) 
	{
		size += baseSize * r;
		size = Math.max(size, minSize);
		size = Math.min(size, maxSize);
	}
	
	public void update()//long deltaTime) 
	{
		draw();
		move();
		actions.Act(new Vect3((-1.01 * size) / Main.canvas.width(),0,0),-1);
		actions.Act(new Vect3((1.01 * size)/ Main.canvas.width(),0,0),1);
		//debug();
	}
	
	public Color read(Vect3 offset)
	{
		Vect3 dir = new Vect3(Math.cos(orientation),-Math.sin(orientation),0);
		
		Color c = Main.canvas.read(position.x + (dir.x * offset.x),position.y + (dir.y * offset.y));
		
		sight = new Color(c.getRed(), c.getGreen(),c.getBlue() );
		
		return sight;
	}
	
	public Bot(String img) throws IOException
	{
		this(ImageIO.read(new File(img)));
	}
	public Bot(BufferedImage img)
	{
		position = new Vect3(0.5f,0.5f,0);
		speed = new Vect3(0,baseSpeed,0);
		orientation = 0;
		size = minSize;
		
		sight = new Color(0,0,0);
		
		actions = new Actions(this,img);
	}
	
	public static float baseSize = 1.25f;
	
	public static int minSize = 25;
	public static int maxSize = 100;
	
	public static float hueStep = 2;
	
	public static float saturationStep = 0.5f;
	int saturationDir = 1;
	
	public static double baseSpeed = 0.0007;
	public static double baseRotation = 0.05;
}
