package bot;

import java.awt.Color;
import java.awt.image.BufferedImage;

import utilities.HSLColor;
import utilities.Vect3;

public class Actions {
	
	int[] matrix;
	int width;
	Bot bot;
	
	Vect3 HSL(Vect3 color)
	{
		Vect3 res = new Vect3(0,0,0);
		
		res.x = Math.acos(
				(color.x - (0.5 * color.y)- (0.5*color.z))/
				(Math.sqrt(
						(color.x*color.x)+(color.y*color.y)+(color.z*color.z)-
						(color.x*color.y)-(color.x*color.z)-(color.y*color.z)))
				);
		
		if(color.y < color.z)
		{
			res.x = 360 - res.x;
		}
		
		res.x /= 360.0;
		
		double max = Math.max(color.x, Math.max(color.y,color.z));
		double min = Math.min(color.x, Math.min(color.y,color.z));
		double d = (max-min);
		
		res.z = (0.5*(max+min));
		
		res.y = res.z > 0 ? d/(1-((2*res.z)-1)) : 0;
		
		return res;
	}
	
	public void Act(Vect3 offset,float coef)
	{
		float[] c = HSLColor.fromRGB(bot.read(offset));
		
		Vect3 v = new Vect3(c[0]/256f,c[1]/256f,c[2]/256f);//HSL(bot.read(offset));
		
		int action = getInMatrix(v.x * (width-1),v.y * (width-1));
		
		//System.out.println("Action : " + action + "  <<  " + v.x + " , " + v.y);
		
		switch(Math.abs(action))
		{
			default:break;
			//case 0: bot.accelerate(v.z); break;
			case 0: bot.rotate(v.z * coef); break;
			case 1: 
				if(coef > 0) {
					bot.changeColor(v.z,0); 
				}else {
					bot.changeColor(v.z,1); 
				}
				break;
			case 2: bot.changeSize(v.z * coef);break;
		}
		
		/*System.out.println(	(action == 0 ? "Rotate" :
							action == 1 ? "Color" : "Size") + "  >> " + v.z);*/
	}
	
	int getInMatrix(double x, double y) { return getInMatrix((int)Math.round(x),(int)Math.round(y)); }
	int getInMatrix(int x, int y)
	{
		return matrix[x + (y*width)];
	}
	
	public Actions(Bot _bot)
	{
		bot = _bot;
		matrix = DefaultMatrix();
	}
	
	public Actions(Bot _bot,BufferedImage _matrix)
	{
		bot = _bot;
		matrix = Matrix(_matrix);
	}
	
	int[] Matrix(BufferedImage _matrix)
	{
		int[] m = new int[_matrix.getWidth() * _matrix.getHeight()];
		width = _matrix.getWidth();
		
		for(int x = 0; x < _matrix.getWidth();x++)
		{
			for(int y = 0 ; y < _matrix.getHeight();y++)
			{
				Color p = new Color(_matrix.getRGB(x,y));
				
				m[x + (y*_matrix.getWidth())] =
						p.getRed() > 2 ? 0 :				
						p.getGreen() > 2 ? 1 : 
											2;						
			}
		}
		
		return m;
	}
	
	int[] DefaultMatrix()
	{
		int[] arr = new int[16];
		width = 4;
		
		arr[0] = 0;arr[1] = -3;arr[2] = 1; arr[3] = 1;
		arr[4] = 1;arr[5] = 3;arr[6] = 2; arr[7] = 0;
		arr[8] = 2;arr[9] = 1;arr[10] = 1; arr[11] = 0;
		arr[12] = 0;arr[13] = 0;arr[14] = 0; arr[15] = 0;
		
		return arr;
	}

}
