package utilities;

public class Vect3 {
	public double x,y,z;
	
	public double lenght()
	{
		return Math.sqrt(x*x + y*y + z*z);
	}
	
	public Vect3()
	{
		
	}
	
	public Vect3(double _x, double _y, double _z)
	{
		x = _x; y = _y; z = _z;
	}
	
	public static Vect3 Add(Vect3 a, Vect3 b)
	{
		return new Vect3(a.x + b.x, a.y+ b.y, a.z + b.z);
	}	
	
	public static Vect3 Mul(Vect3 a,double b)
	{
		return new Vect3(a.x * b, a.y * b, a.z * b);
	}
	
	public Vect3 mul(double b)
	{
		x *= b; y*= b; z *= b;
		return this;
	}
	
	public Vect3 add(Vect3 b)
	{
		x += b.x; y+= b.y; z += b.z;
		return this;
	}
}
