package image;

import java.io.File;
import java.io.IOException;
import java.awt.*;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import utilities.Vect3;


public class Canvas {

	BufferedImage image; public BufferedImage Image() {return image;}
	Graphics2D graphics;

	public Color read(double x, double y)
	{
		return read((int)Math.round(x * (image.getWidth()-1)),(int)Math.round(y * (image.getHeight()-1)));
	}
	public Color read(int x, int y)
	{	
		x = Math.min(Math.max(x, 0),image.getWidth()-1);
		y = Math.min(Math.max(y, 0),image.getHeight()-1);
		Color c = new Color(image.getRGB(x,y));
		
		return c;
	}
	
	public void put(double x, double y, Color c, int size, Vect3 speed)
	{
		put(
				(int)Math.round(x * (image.getWidth()-1)),// - (Math.signum(speed.x) * size * 0.5f)),
				(int)Math.round(y * (image.getHeight()-1)),// - (Math.signum(speed.y) * size* 0.5f)),
				c,size);
	}
	public void put(int x, int y, Color c) {put(x,y,c,3);}
	public void put(int x, int y, Color c,int size)
	{		
		//System.out.println(" >> " + x + " , " + y);
		graphics.setColor(c); 
		graphics.fillOval(x,y,size,size);
	}
	
	
	public Canvas(String path) throws IOException
	{
		image = ImageIO.read(new File(path));
		graphics = (Graphics2D) image.getGraphics();
	}
	
	public int width() {return image.getWidth();}
}
