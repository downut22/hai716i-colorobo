using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour
{
    public Transform hand;

    public Vector3 speed;
    public Vector3 rotationSpeed;

    public Vector2 maxBounds;
    public Vector2 minBounds;

    public Transform armParent;

    public LineRenderer arm;

    void FixedUpdate()
    {
        Out();

        UpdateHand();
        try
        {
            Color c = Read();
            Act(c);
        }
        catch(System.Exception e)
        {
            
        }

        arm.SetPosition(0, armParent.position);
        arm.SetPosition(1, hand.position);

    }

    void Out()
    {

        if (hand.localPosition.x < minBounds.x)
        { 
            speed.x = -speed.x;
            hand.localPosition = new Vector3(minBounds.x+0.01f, hand.localPosition.y, hand.localPosition.z);
        }
        if(hand.localPosition.x > maxBounds.x) 
        { 
            speed.x = -speed.x;
            hand.localPosition = new Vector3(maxBounds.x-0.01f, hand.localPosition.y, hand.localPosition.z);
        }

        if (hand.localPosition.y < minBounds.y)
        {
            speed.y = -speed.y;
            hand.localPosition = new Vector3(hand.localPosition.x, minBounds.y+0.01f, hand.localPosition.z);
        }
        if (hand.localPosition.y > maxBounds.y)
        {
            speed.y = -speed.y;
            hand.localPosition = new Vector3(hand.localPosition.x,maxBounds.y-0.01f, hand.localPosition.z);
        }
    }

    void Act(Color c)
    {
        if(c.r < 0.1f)
        {
            speed = Quaternion.Euler(rotationSpeed) * speed;
            /*float t = speed.x;
            speed.x = speed.y;
            speed.y = t;*/
        }
    }

    void UpdateHand()
    {
        hand.Translate(speed);
    }

    Color Read()
    {
        Color c = canvas.Read(hand);return c;
    }

    public Canvas canvas;
}
