using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    public float moveSpeed;
    
    void Update()
    {
        if(Input.GetKey(KeyCode.Mouse0))
        {
            transform.Translate(Input.GetAxis("Mouse X")* moveSpeed, Input.GetAxis("Mouse Y")* moveSpeed, 0);
        }

        Cursor.visible = false;
    }
}
