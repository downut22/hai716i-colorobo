using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canvas : MonoBehaviour
{
    [Header("Properties")]
    public LayerMask layerMask;
    public float pencilDistance;
    public float pencilSize;
    public Color pencilColor;
    public float canvasSize;
    
    [Space(10)]

    public Pencil[] pencils;

    public int drawPerFrame;
    Queue<DrawPoint> toDraw;

    public Texture2D texture;

    void Start()
    {
        toDraw = new Queue<DrawPoint>();

        foreach (Pencil p in pencils)
        {
            p.pencilPoint = RayTrace(p.transform);
            p.lastPoint = p.pencilPoint;
        }
    }

    void Update()
    {
        foreach (Pencil p in pencils)
        {
            p.pencilPoint = RayTrace(p.transform);
            if (p.pencilPoint != default && p.pencilPoint != p.lastPoint)
            {
                Draw(p);
                p.lastPoint = p.pencilPoint;
            }
        }

        if(toDraw.Count > 0)
        {
            for(int i = 0; i < drawPerFrame && toDraw.Count > 0;i++)
            {
                DrawPoint p = toDraw.Dequeue();
                Draw(p.point.x, p.point.y, p.color);
            }
        }
    }

    public Color Read(Transform t)
    {
        Vector3 p = RayTrace(t); if(p == default) { throw new System.Exception(); }

        Vector3 a = GetPos(p);
        return texture.GetPixel((int)a.x,(int)a.y);
    }

    Vector3 GetPos(Vector3 point)
    {
        point -= transform.position;
        point /= canvasSize;
        point *= texture.width;
        return point;
    }

    void Draw(Pencil p)
    {
        Vector3 target = GetPos(p.pencilPoint);
        Vector3 from = GetPos(p.lastPoint);

       // Debug.Log("Drawing at " + p.pencilPoint);

        float t = (target - from).magnitude / pencilSize;
        Vector3 d = (target - from).normalized * pencilSize ;

        for (int i = 0; i < t;i++)
        {
            Vector3 pp = from + (i * d);
            for (int x = 0; x < pencilSize; x++)
            {
                for (int y = 0; y < pencilSize; y++)
                {
                    toDraw.Enqueue(new DrawPoint(new Vector3Int((int)pp.x + x, (int)pp.y + y),p.color));
                }
            }
        }
    }

    void Draw(int x, int y, Color color)
    {
        if (x >= 0 && x < texture.width && y >= 0 && y < texture.height)
        {
            texture.SetPixel(x, y, color);
            texture.Apply();
        }
    }

    public struct DrawPoint
    {
        public Vector3Int point;
        public Color color;
        public DrawPoint(Vector3Int p, Color c) { point = p; color = c; }
    }

    Vector3 RayTrace(Transform t)
    {
        RaycastHit hit;
        Debug.DrawRay(t.position, t.forward * pencilDistance * 10, Color.red);
        if (Physics.Raycast(t.position, t.forward, out hit, pencilDistance, layerMask))
        {
            return hit.point;
        }
        else
        {
            return default;
        }
    }

    [System.Serializable]
    public class Pencil
    {
        public Transform transform;
        public Vector3 lastPoint;
        public Vector3 pencilPoint;
        public Color color;
    }
}

