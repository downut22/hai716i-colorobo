﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Bot::FixedUpdate()
extern void Bot_FixedUpdate_m84C47B888B0E8223160ED001E5BA0593C8766FF2 (void);
// 0x00000002 System.Void Bot::Out()
extern void Bot_Out_mEAE632A36CAC599EB00ADC9152EE2E22727D812F (void);
// 0x00000003 System.Void Bot::Act(UnityEngine.Color)
extern void Bot_Act_m0E8449AADFA109C5DE1B11FEC52E77C68F0F9000 (void);
// 0x00000004 System.Void Bot::UpdateHand()
extern void Bot_UpdateHand_m2E1A7BD65D108A2939EF0C14B5ECFE9327757E32 (void);
// 0x00000005 UnityEngine.Color Bot::Read()
extern void Bot_Read_mAD6330CE5576EB9F11BA100789EEAD502768D4DF (void);
// 0x00000006 System.Void Bot::.ctor()
extern void Bot__ctor_m721C9F7B447E4CD482724E545EE797CD7762B2D8 (void);
// 0x00000007 System.Void Canvas::Start()
extern void Canvas_Start_m76706FDD067B93C11CB4F02E405D5D04360DA4E9 (void);
// 0x00000008 System.Void Canvas::Update()
extern void Canvas_Update_mC3A61EAD58259083CEA35EE6DD681A48D3961F9C (void);
// 0x00000009 UnityEngine.Color Canvas::Read(UnityEngine.Transform)
extern void Canvas_Read_m973446C57A5C16E95B4C010EE4D725580D2201C8 (void);
// 0x0000000A UnityEngine.Vector3 Canvas::GetPos(UnityEngine.Vector3)
extern void Canvas_GetPos_mE63328F0490D95C4935E7C762FE008B16707CEF7 (void);
// 0x0000000B System.Void Canvas::Draw(Canvas/Pencil)
extern void Canvas_Draw_mB1D1B5F10E0C15033FE512128A14A3F888A41913 (void);
// 0x0000000C System.Void Canvas::Draw(System.Int32,System.Int32,UnityEngine.Color)
extern void Canvas_Draw_m961D617B0890E36797CCA89AA5F4D3F6DDD9847C (void);
// 0x0000000D UnityEngine.Vector3 Canvas::RayTrace(UnityEngine.Transform)
extern void Canvas_RayTrace_m3316F52477B91A225C8F47FE57CC4C7061DA6536 (void);
// 0x0000000E System.Void Canvas::.ctor()
extern void Canvas__ctor_m3A1F615E32ABBD3213E0F6F2A1D1F610A9DB0E3C (void);
// 0x0000000F System.Void Canvas/DrawPoint::.ctor(UnityEngine.Vector3Int,UnityEngine.Color)
extern void DrawPoint__ctor_mB4211C6C8C844789C372C1BE673AE1A135712D38 (void);
// 0x00000010 System.Void Canvas/Pencil::.ctor()
extern void Pencil__ctor_m2BEB82ED23E6DF28D35BBE2729565E3265B6B308 (void);
// 0x00000011 System.Void Control::Update()
extern void Control_Update_mCE2B2E5B7A43ED1E76ED9B3F321212A01283F8C7 (void);
// 0x00000012 System.Void Control::.ctor()
extern void Control__ctor_mEAB84F82268EB35BBA7634ED456BBA8EF3FDDFDB (void);
// 0x00000013 System.Void Unity.Template.VR.XRPlatformControllerSetup::Start()
extern void XRPlatformControllerSetup_Start_m1F22FCA29DFD83DC0E343C3F391D04A7C52085BF (void);
// 0x00000014 System.Void Unity.Template.VR.XRPlatformControllerSetup::.ctor()
extern void XRPlatformControllerSetup__ctor_mF9A3998AF90962CF8F35BAF2221558BDF5F6596E (void);
static Il2CppMethodPointer s_methodPointers[20] = 
{
	Bot_FixedUpdate_m84C47B888B0E8223160ED001E5BA0593C8766FF2,
	Bot_Out_mEAE632A36CAC599EB00ADC9152EE2E22727D812F,
	Bot_Act_m0E8449AADFA109C5DE1B11FEC52E77C68F0F9000,
	Bot_UpdateHand_m2E1A7BD65D108A2939EF0C14B5ECFE9327757E32,
	Bot_Read_mAD6330CE5576EB9F11BA100789EEAD502768D4DF,
	Bot__ctor_m721C9F7B447E4CD482724E545EE797CD7762B2D8,
	Canvas_Start_m76706FDD067B93C11CB4F02E405D5D04360DA4E9,
	Canvas_Update_mC3A61EAD58259083CEA35EE6DD681A48D3961F9C,
	Canvas_Read_m973446C57A5C16E95B4C010EE4D725580D2201C8,
	Canvas_GetPos_mE63328F0490D95C4935E7C762FE008B16707CEF7,
	Canvas_Draw_mB1D1B5F10E0C15033FE512128A14A3F888A41913,
	Canvas_Draw_m961D617B0890E36797CCA89AA5F4D3F6DDD9847C,
	Canvas_RayTrace_m3316F52477B91A225C8F47FE57CC4C7061DA6536,
	Canvas__ctor_m3A1F615E32ABBD3213E0F6F2A1D1F610A9DB0E3C,
	DrawPoint__ctor_mB4211C6C8C844789C372C1BE673AE1A135712D38,
	Pencil__ctor_m2BEB82ED23E6DF28D35BBE2729565E3265B6B308,
	Control_Update_mCE2B2E5B7A43ED1E76ED9B3F321212A01283F8C7,
	Control__ctor_mEAB84F82268EB35BBA7634ED456BBA8EF3FDDFDB,
	XRPlatformControllerSetup_Start_m1F22FCA29DFD83DC0E343C3F391D04A7C52085BF,
	XRPlatformControllerSetup__ctor_mF9A3998AF90962CF8F35BAF2221558BDF5F6596E,
};
extern void DrawPoint__ctor_mB4211C6C8C844789C372C1BE673AE1A135712D38_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x0600000F, DrawPoint__ctor_mB4211C6C8C844789C372C1BE673AE1A135712D38_AdjustorThunk },
};
static const int32_t s_InvokerIndices[20] = 
{
	4934,
	4934,
	3845,
	4934,
	4746,
	4934,
	4934,
	4934,
	2955,
	3612,
	3928,
	1040,
	3610,
	4934,
	2220,
	4934,
	4934,
	4934,
	4934,
	4934,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	20,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
